# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Ejemplo de Data Loss Prevention para buscar nombres y datos personalizados."""

#from __future__ import print_function

import google.cloud.dlp

def dlp_custom_detection():

    # Google Cloud Project ID.
    project = 'myxerticalabsproject'

    dlp = google.cloud.dlp.DlpServiceClient()

    # la cadena a inspeccionar
    content = 'Antonio Palacios Chrem tiene registrado el rfc: PACA681002ag9 y la CURP: PACA681002HDFLHN01'

    item = {'value': content}

    # info_types a buscar en la cadena que es inspeccionada. Existe una lista de elementos predefinidos por la API
    info_types = [{'name': 'FIRST_NAME'}, {'name': 'LAST_NAME'}, {'name': 'MEXICO_CURP_NUMBER'}]

    # La probabilidad inferior aceptable para indicar que se ha encontrado una coincidencia.
    # Los posibles valores son 'LIKELIHOOD_UNSPECIFIED', 'VERY_UNLIKELY', 'UNLIKELY',
    # 'POSSIBLE', 'LIKELY', 'VERY_LIKELY'
    min_likelihood = 'POSSIBLE'

    # El valor superior de hallazgos que son reportados (0 = server maximum)
    max_findings = 0

    # True cuando se quiera incluir la cadena identificada en el resultado
    include_quote = True

    # Valores personalizados de info types a identificar
    my_custom_info_type_dictionary = ['RFC', 'curp', 'registrado']
    my_custom_info_type_regex = '([aA-zZ]{4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[aA-zZ|\d]{3})'

    inspect_config = {
        'info_types': info_types,
        'custom_info_types': [{
            'info_type': {
                'name': 'MY_CUSTOM_DICTIONARY'
            },
            'dictionary': {
                'word_list': {
                    'words': my_custom_info_type_dictionary
                }
            }
        },
            {
                'info_type': {
                    'name': 'RFC_REGEX'
                },
                'regex': {
                    'pattern': my_custom_info_type_regex
                }
            }
        ],
        'min_likelihood': min_likelihood,
        'include_quote': include_quote,
        'limits': {'max_findings_per_request': max_findings},
    }

    parent = dlp.project_path(project)

    # llamado a la API.
    response = dlp.inspect_content(parent, inspect_config, item)

    # Imprimir la respuesta
    if response.result.findings:
        for finding in response.result.findings:
            try:
                print('Hallazgo: {}'.format(finding.quote))
            except AttributeError:
                pass
            print('Info type: {}'.format(finding.info_type.name))
            likelihood = (google.cloud.dlp_v2.types.Finding.DESCRIPTOR
                          .fields_by_name['likelihood']
                          .enum_type.values_by_number[finding.likelihood]
                          .name)
            print('Likelihood: {}'.format(likelihood))
    else:
        print('Sin hallazgos')


if __name__ == '__main__':
    dlp_custom_detection()
