# Google Cloud Functions 

Un ejemplo de una Google Cloud Function desarrollada en Python. Tiene por objeto desencadenar el análisis de la 
información contenida en archivos que son escritos en Google Cloud Storage. El análisis de la información se hace con 
Google Cloud DLP API 

## Automatiza el análisis de tus archivos mediante Google Cloud Functions y Data Loss Prevention API

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

* Crea tu entorno virtual

```
virtualenv -p python3 my_cf_venv
source my_cf_venv/bin/activate
```

##### 4.

* Resuelve tus dependencias

```
pip install -r requirements.txt
``` 

##### 5. 

* [Crea](https://console.cloud.google.com/iam-admin/serviceaccounts) una cuenta de servicio con los 
privilegios mínimos necesarios y obtén el correspondiente archivo .json

* Asigna a la variable `GOOGLE_APPLICATION_CREDENTIALS` la ruta de tu archivo .json
  
Ejemplo para Linux y MacOS
```bash
export GOOGLE_APPLICATION_CREDENTIALS="/La/Ruta/De/Tu/Archivo.json"
```
Ejemplo para Windows Power Shell
```
$env:GOOGLE_APPLICATION_CREDENTIALS="C:\La\Ruta\De\Tu\Archivo.json
```

##### 6. 
* Despliega tu endpoint

```bash
gcloud beta functions deploy dlp_file_detection --runtime python37 --trigger-resource EL_NOMBRE_DE_TU_BUCKET 
--trigger-event google.storage.object.finalize
```

* Si todo salió bien ahora puedes probar tu función subiendo un archivo. Asegúrate que el archivo incluya entre todo su 
contenido algún nombre o apellido para que notes en los logs cuando DLP los ha detectado. 
Sube tu archivo a Cloud Storage mediante la consola web de tu proyecto o ejecutando el comando: 

```bash
gsutil cp LA_RUTA_DE_TU_ARCHIVO gs://EL_NOMBRE_DE_TU_BUCKET/
```

#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)