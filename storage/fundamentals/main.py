# Copyright 2016 Google, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Tomado de https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/storage/cloud-client/encryption.py

import argparse
import base64
import os

from google.cloud import storage


def generate_encryption_key():
    # Genera una AES Key encryption de 256 bit (32 byte) e imprime su equivalente en base64.
    key = os.urandom(32)
    encoded_key = base64.b64encode(key).decode('utf-8')
    print('Key encryption encoded en Base 64: {}'.format(encoded_key))


def upload_encrypted_blob(bucket_name, source_file_name,
                          destination_blob_name, base64_encryption_key):

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    encryption_key = base64.b64decode(base64_encryption_key)
    blob = storage.Blob(destination_blob_name, bucket, encryption_key=encryption_key)

    blob.upload_from_filename(source_file_name)

    print('Archivo {} subido: {}.'.format(
        source_file_name,
        destination_blob_name))


def download_encrypted_blob(bucket_name, source_blob_name,
                            destination_file_name, base64_encryption_key):

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    encryption_key = base64.b64decode(base64_encryption_key)
    # No podrás obtener el archivo si no pasas la encryption_key
    blob = storage.Blob(source_blob_name, bucket, encryption_key=encryption_key)

    blob.download_to_filename(destination_file_name)

    print('Blob {} descargado: {}.'.format(
        source_blob_name,
        destination_file_name))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers(dest='command')

    subparsers.add_parser(
        'generate-encryption-key', help=generate_encryption_key.__doc__)

    upload_parser = subparsers.add_parser(
        'upload', help=upload_encrypted_blob.__doc__)
    upload_parser.add_argument(
        'bucket_name', help='El nombre de tu bucket en cloud storage.')
    upload_parser.add_argument('source_file_name')
    upload_parser.add_argument('destination_blob_name')
    upload_parser.add_argument('base64_encryption_key')

    download_parser = subparsers.add_parser(
        'download', help=download_encrypted_blob.__doc__)
    download_parser.add_argument(
        'bucket_name', help='El nombre de tu bucket en cloud storage.')
    download_parser.add_argument('source_blob_name')
    download_parser.add_argument('destination_file_name')
    download_parser.add_argument('base64_encryption_key')

    args = parser.parse_args()

    if args.command == 'generate-encryption-key':
        generate_encryption_key()
    elif args.command == 'upload':
        upload_encrypted_blob(
            args.bucket_name,
            args.source_file_name,
            args.destination_blob_name,
            args.base64_encryption_key)
    elif args.command == 'download':
        download_encrypted_blob(
            args.bucket_name,
            args.source_blob_name,
            args.destination_file_name,
            args.base64_encryption_key)
